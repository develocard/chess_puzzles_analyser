"""
Author: Starmania

Get the number of users that have solved a chess puzzle on chess.com for each day since 2007-05-08

Returns:
    chess_com.csv (file): A csv file with the date and the number of users that have solved a chess puzzle on chess.com
"""

import datetime
from dataclasses import dataclass
from typing import List, Union
from hashlib import sha1

import requests
from alive_progress import alive_bar

URL = "https://www.chess.com/callback/puzzles/daily?start={}&end={}"
START_DATE = "2007-05-08"
DAYS_PAGING = 180


@dataclass
class Puzzle:  # pylint: disable=missing-class-docstring
    puzzle_id: int
    title: str
    date: str
    pgn: str
    comment_count: int = 0
    solved_count: int = 0

    def __str__(self):
        return "".join([f"id: {str(self.puzzle_id)}, date: {self.date}, ",
                        f"title: {self.title}, pgn: {sha1(self.pgn.encode()).digest().hex()[0:7]}"])

    def __eq__(self, other) -> bool:
        return bool(self.puzzle_id == other.puzzle_id and self.date == other.date and self.title == other.title)

    def __ne__(self, other):
        return not self.__eq__(other)


def analyze_puzzles(puzzles_list: List[Puzzle]) -> None:
    """Search for duplicates in the puzzles list

    Args:
        puzzles_list (List[Puzzle]): List of puzzles
    """
    puzzle_dates_list = list(puzzle.date for puzzle in puzzles_list)
    duplicates_date = list_duplicates(puzzle_dates_list)
    same_date_puzzles = list(puzzle for puzzle in puzzles_list if puzzle.date in duplicates_date)
    print(f"Nombre total de puzzles : {len(puzzles_list)}")
    print(f"Puzzle à la même date ({len(same_date_puzzles)}) :")
    for puzzle in same_date_puzzles:
        print(str(puzzle))

    # Same, but for pgn
    puzzle_pgn_list = list(puzzle.pgn for puzzle in puzzles_list)
    duplicates_pgn = list_duplicates(puzzle_pgn_list)
    same_pgn_puzzles = list(puzzle for puzzle in puzzles_list if puzzle.pgn in duplicates_pgn)
    print(f"Nombre total de pgn : {len(puzzle_pgn_list)}")
    print(f"Puzzle avec le même pgn ({len(same_pgn_puzzles)}) :")

    same_pgn_puzzles.sort(key=lambda x: x.pgn)
    for puzzle in same_pgn_puzzles:
        print(str(puzzle))


def clean_list(list_to_clean: list) -> list:
    """Remove duplicates from a list

    Args:
        list_to_clean (list): List to clean

    Returns:
        list: Cleaned list
    """
    cleaned_list = []
    for elem in list_to_clean:
        if elem not in cleaned_list:
            cleaned_list.append(elem)
    return cleaned_list


def clean_text(text: str) -> str:
    """Clean a text for csv

    Args:
        text (str): Text to clean

    Returns:
        str: Cleaned text
    """
    text = text.replace(',', ' ')
    text = text.replace(';', ' ')
    text = text.replace('\r', '')
    text = text.replace('\n', '')
    return text


def get_puzzles_data(start_date: str, end_date: str) -> List[Puzzle]:
    """Get the data from the API"""
    puzzles_list = []
    url = URL.format(start_date, end_date)
    response = requests.get(url, timeout=5)
    if response.status_code == 200:
        for data in response.json():
            puzzle = Puzzle(data["id"], data["title"], data["date"], data["pgn"], data["comment_count"],
                            data["solved_count"])
            puzzles_list.append(puzzle)
    return puzzles_list


def get_puzzles_list(start_date: datetime, end_date: datetime) -> List[Puzzle]:
    """Get the list of puzzles between two dates

    Args:
        start_date (datetime)
        end_date (datetime)

    Returns:
        List[Puzzle]: List of puzzles
    """
    current = start_date
    puzzles_list = []
    total_days = (end_date.date() - current.date()).days
    with alive_bar(manual=True) as bar:
        while current.date() < end_date.date():
            end = current + datetime.timedelta(days=DAYS_PAGING)
            end = min(end, end_date)

            # Query the puzzles and add them to the list
            puzzles_list.extend(get_puzzles_data(current.strftime("%Y-%m-%d"), end.strftime("%Y-%m-%d")))
            current = end
            done = (end_date.date() - current.date()).days
            percent = (total_days - done) / total_days
            bar(percent)  # pylint: disable=not-callable
    return puzzles_list


def list_duplicates(elements_list: list) -> list:
    """Return a list of duplicates in a list

    Args:
        elements_list (list): List to search

    Returns:
        list: List of duplicates
    """
    seen = set()
    duplicates = [x for x in elements_list if x in seen or seen.add(x)]
    return duplicates


def save_puzzles_data(puzzles_list: List[Puzzle], sort_by: Union[str, None] = None,
                      output: str = "chess_com.csv") -> None:
    """Take a puzzles list and save it to a csv file

    Args:
        puzzles_list (List[Puzzle]): List of puzzles
        sort_by (Union[str, None], optional): The field that sorts the output file. Defaults to None.
        output (str, optional): Output file name. Defaults to "chess_com.csv".

    Returns:
        None, create a file
    """
    if sort_by and sort_by in Puzzle.__annotations__.keys():
        puzzles_list.sort(key=lambda x: getattr(x, sort_by))

    with open(output, "w", encoding="utf-8") as file:
        file.write(",".join(["id", "date", "solved_count", "comment_count", "title", "\n"]))
        for puzzle in puzzles_list:
            title = clean_text(puzzle.title)
            date = clean_text(puzzle.date)
            file.write(",".join(
                [str(puzzle.puzzle_id), date, str(puzzle.solved_count),
                 str(puzzle.comment_count), title, "\n"]))


def generate_dates(start_date: str, end_date: str) -> List[datetime.date]:
    """Generate a list of dates between two dates

    Args:
        start_date (str): Start date
        end_date (str): End date

    Returns:
        List[datetime.date]: List of dates
    """
    start = datetime.datetime.strptime(start_date, "%Y-%m-%d")
    end = datetime.datetime.strptime(end_date, "%Y-%m-%d")
    date_generated = [start + datetime.timedelta(days=x) for x in range(0, (end - start).days)]
    return date_generated


def main() -> None:  # pylint: disable=missing-function-docstring
    puzzles_list = get_puzzles_list(datetime.datetime.strptime(START_DATE, "%Y-%m-%d"), datetime.datetime.now())
    puzzles_list = clean_list(puzzles_list)

    analyze_puzzles(puzzles_list)

    save_puzzles_data(puzzles_list)
    save_puzzles_data(puzzles_list, "solved_count", "sorted_chess_com.csv")


if __name__ == "__main__":
    main()
