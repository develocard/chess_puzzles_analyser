# Chess Puzzles Analyser

## Usage

Get the number of users that have solved a chess puzzle on chess.com for each day since 2007-05-08

Returns:
    chess_com.csv (file): A csv file with the date and the number of users that have solved a chess puzzle on chess.com
    sorted_chess_com.csv (file): Same csv file, but sorted by solved count.

## Author information

This code was created in 2023 by Starmania and forked by Develocard.
